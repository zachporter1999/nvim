return {
    "ribelo/taskwarrior.nvim",
    config = true,
    keys = {
        {
            "<leader>r",
            'lua require("taskwarrior_nvim").browser({"ready"})<cr>',
            desc = 'Taskwarrior',
        },
    },
}
