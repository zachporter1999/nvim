return {
    {
        'nvim-telescope/telescope.nvim',
        lazy = false,
        dependencies = { 'nvim-lua/plenary.nvim', },
        opts = {
            pickers = {
                buffers = {
                    theme = 'ivy',
                },
                find_files = {
                    theme = 'ivy',
                },
                live_grep = {
                    theme = 'dropdown',
                },
            },
        },
        keys = {
            {
                '<leader>tf',
                ':Telescope find_files<cr>',
                desc = 'Find Files',
            },
            {
                '<leader>tb',
                ':Telescope buffers<cr>',
                desc = 'Buffers',
            },
            {
                '<leader>tg',
                ':Telescope live_grep<cr>',
                desc = 'Live Grep',
            },
        },
    },
}
