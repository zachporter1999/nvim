return {
    {
        'nvim-treesitter/nvim-treesitter',
        opts = {
            ensure_installed = {
                "haskell",
                "groovy",
                "org",
                "markdown",
                "markdown_inline",
                "c",
                "cpp",
                "python",
                "rust",
                "lua",
            },
            sync_install = true,
            auto_install = true,
            highlight = {
                enable = true,
            },
        },
        config = function()
            vim.api.nvim_command(":TSUpdate")
            vim.api.nvim_command(":TSEnable highlight")
        end,
    },
}
