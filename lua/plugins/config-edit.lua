return {
    {
        'nvim-telescope/telescope.nvim',
        lazy = false,
        dependencies = { 'nvim-lua/plenary.nvim', },
        init = function()
            local pickers = require 'telescope.pickers'
            local finders = require 'telescope.finders'
            local conf = require'telescope.config'.values
            local actions = require 'telescope.actions'
            local action_state = require 'telescope.actions.state'

            local get_loaded_configs = function ()
                print("Test")
                local config_list = {}
                for k, v in pairs(package.loaded) do
                    if string.find(k, "plugins/") then
                        table.insert(config_list, k)
                    end
                end
                return config_list
            end

            function conf_unloader (opts)
                opts = opts or {}

                pickers.new(opts, {
                    prompt_title = "Loaded Configs",
                    finder = finders.new_table {
                        results = get_loaded_configs(),
                    },
                    attach_mappings = function (prompt_bufnr, map)
                        actions.select_default:replace(function ()
                            actions.close(prompt_bufnr)
                            local selection = action_state.get_selected_entry()
                            package.loaded[selection[1]] = nil
                        end)
                        return true
                    end,
                    sorter = conf.generic_sorter(opts),
                }):find()
                vim.api.nvim_command("source " .. vim.fn.stdpath('config') .. "/init.lua")
            end

            vim.api.nvim_create_user_command(
                'ConfigEditAny',
                'cd ' .. vim.fn.stdpath('config') .. '/lua/config | Telescope find_files',
                { nargs = '*' })
            vim.api.nvim_create_user_command(
                'ConfigEditInit',
                'tabnew ' .. vim.fn.stdpath('config') .. '/init.lua',
                { nargs = '*' })
            vim.api.nvim_create_user_command(
                'ConfigSource',
                'source ' .. vim.fn.stdpath('config') .. '/init.lua',
                { nargs = '*' })
            vim.api.nvim_create_user_command(
                "ConfigUnload",
                "lua conf_unloader()",
                { nargs = '*' })
        end,
        keys = {
            {
                '<leader>ci',
                ':ConfigEditInit<cr>',
                desc = "edit config",
            },
            {
                '<leader>ce',
                ':ConfigEditAny<cr>',
                desc = "edit config",
            },
            {
                '<leader>cs',
                ':ConfigSource<cr>',
                desc = "Source Config",
            },
            {
                '<leader>cu',
                ':Unload<cr>',
                desc = "Unload Config",
            },
            -- Lazy keys
            {
                '<leader>lh',
                ':Lazy home<cr>',
                --desc = "Unload Config",
            },
            {
                '<leader>lu',
                ':Lazy update<cr>',
                --desc = "Unload Config",
            },
        },
    },
}
