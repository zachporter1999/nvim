return {
    'ttbug/tig.nvim',
    lazy = false,
    keys = {
        {
            "<leader>g",
            ":lua require('tig').open()<cr>",
            desc = "Tig",
        },
    },
}
