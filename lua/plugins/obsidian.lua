return {
    {
        'epwalsh/obsidian.nvim',
        version = "*",
        ft = "markdown",
        dependencies = {
            "nvim-lua/plenary.nvim",
        },
        opts = {
            workspaces = {
                {
                    name = "test",
                    path = "~/.vaults/test"
                },
            },
            templates = {
                subdir = "templates",
            },
            daily_notes = {
                folder = "daily notes",
                template = "daily-template",
            },
            mappings = {
                ["gf"] = {
                    action = function()
                        return require("obsidian").util.gf_passthrough()
                    end,
                    opts = { noremap = false, expr = true, buffer = true },
                },
                ["ch"] = {
                  action = function()
                    return require("obsidian").util.toggle_checkbox()
                  end,
                  opts = { buffer = true },
                },
               -- Smart action depending on context, either follow link or toggle checkbox.
                ["<cr>"] = {
                  action = function()
                    return require("obsidian").util.smart_action()
                  end,
                  opts = { buffer = true },
                },
                ["lb"] = {
                    action = function()
                        return vim.cmd("ObsidianBacklinks")
                    end,
                    opts = { noremap = false, expr = true, buffer = true },
                },
                ["lf"] = {
                    action = function()
                        return vim.cmd("ObsidianLinks")
                    end,
                    opts = { noremap = false, expr = true, buffer = true },
                },
                ["tc"] = {
                    action = function()
                        return vim.cmd("ObsidianTOC")
                    end,
                    opts = { noremap = false, expr = true, buffer = true },
                },
            },
        },
    },
}
