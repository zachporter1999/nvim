return {
    {
        "folke/zen-mode.nvim",
        init = function()
            vim.api.nvim_set_keymap('n',
                                    '<leader>z',
                                    ':ZenMode<cr>',
                                    { noremap = true })
        end,
        -- keys = {
        --     {
        --         "<leader>z",
        --         function()
        --             vim.api.nvim_command("ZenMode")
        --         end,
        --         desc = "ZenMode",
        --     },
        -- }
    },
}
