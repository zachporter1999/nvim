# Neovim Config

## Overview

This is my personal neovim config. I utilize packer to make a modular style
configuration; containing the required plugins, and the required setup into a
specific file

## Adding plugins

```lua
add_plugin({ 'folke/which-key.nvim', })
```

## Config Files

Add config to `lua/config/example.lua`


### File structure

```lua
add_plugin({ --[[ Plugin ]] })

local setup = function()
    -- Add any setup here otherwise return nothing
end

-- Local variable or function definitions

return {
    setup = setup
}
```

## Loading config

```lua
map(require_configs, {
    -- General
    'config/example',
})
```

## Loading Plugins

```lua
packer_load_plugins()
```

## Running Setup

```lua
load_config()
```
