""" Nvim Config

" ========================================
" Plugins
" ========================================

call plug#begin()
    Plug 'ixru/nvim-markdown'

    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-telescope/telescope.nvim'

    Plug 'folke/which-key.nvim'
    Plug 'aklt/plantuml-syntax'
call plug#end()

" Set
let g:mapleader=';'
set laststatus=1
set mouse="" " default: nvi (normal, visual, insert)
set tabstop=4
set shiftwidth=4
" Needed for whichkey
set timeout
set timeoutlen=300

" Enabled
set expandtab
set number
set relativenumber
set notermguicolors

" Disabled
set nowrap

" Commands
command -nargs=* -complete=shellcmd  Tig      term tig    <f-args>
command -nargs=* -complete=shellcmd  Duck     term duck   <f-args>
command -nargs=* -complete=shellcmd  Task     term tasksh <f-args>
command -nargs=* -complete=shellcmd  Plantuml plantuml  <f-args>
command                              Notes    e ~/.notes/index.md

"autocmd BufWritePost *.puml,*.plantuml :call system("plantuml -tpng %")
autocmd BufWritePost *.puml,*.plantuml :!plantuml -tpng %

if has('nvim')
    luafile $HOME/.config/nvim/config.lua

    tnoremap <C-w> <C-\><C-n><C-w>
    nnoremap <leader>c :tabnew $HOME/.config/nvim/init.vim<cr>
    nnoremap <leader>s :source $HOME/.config/nvim/init.vim<cr>

    colorscheme retrobox

    " Custom overrides to allow transparency
    "highlight Normal    ctermbg=None guibg=None
    "highlight VertSplit ctermbg=None guibg=None
else
    nnoremap <leader>c :tabnew  $HOME/.vimrc<cr>
    nnoremap <leader>s :source  $HOME/.vimrc<cr>

    colorscheme slate
endif
