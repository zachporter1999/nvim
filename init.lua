vim.api.nvim_command("colorscheme retrobox")
-- Needed for transparency for alacritty?
vim.api.nvim_command("highlight Normal ctermbg=None guibg=None")

-- Set these before loading lazy
vim.g.mapleader      =';'
vim.g.maplocalleader =' '
vim.o.mouse          = ""       -- Disable mouse movement
vim.o.background     = 'dark'

vim.o.tabstop        = 4
vim.o.shiftwidth     = 4
vim.o.laststatus     = 3

vim.o.textwidth      = 80
vim.o.colorcolumn    = '+0'
-- Needed for whichkey
vim.o.timeout        = false
vim.o.timeoutlen     = 10

vim.o.list           = true
vim.o.expandtab      = true
vim.o.number         = true
vim.o.relativenumber = true
vim.o.wrap           = true
vim.o.autochdir      = false
vim.o.cursorline     = true
vim.o.cursorcolumn   = false
vim.o.conceallevel   = 1
vim.o.termguicolors  = false

-- Fix terminal nav to be more like vim
vim.api.nvim_set_keymap(
        't',
        '<C-w>',
        '<C-\\><C-n><C-w>',
        { noremap = true })

-- Make terminal cleaner
vim.api.nvim_create_autocmd({ "TermOpen" }, {
    pattern = { "*" },
    callback = function()
        vim.o.nu    = false
        vim.o.rnu   = false
        vim.o.list  = false
    end,
})

require("config.lazy")
